// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyATXbRpmBu_0HW58wUsKK-YedfEP5gI0wU",
    authDomain: "kebab-king-6ebcd.firebaseapp.com",
    projectId: "kebab-king-6ebcd",
    storageBucket: "kebab-king-6ebcd.appspot.com",
    messagingSenderId: "756555038633",
    appId: "1:756555038633:web:cb666fb6e0e52eefa5ceb8",
    measurementId: "G-1FFKL26SSS"
  }
  
};

export const environmentmap = {
  production: true,
  mapBoxToken: 'pk.eyJ1IjoiZGFuaWxsZXJhIiwiYSI6ImNsMjd0NDR5djAxZXMza3AyNmpuNHpwZzcifQ.q_5c5Wa-rjgImeP_pUnDYw'
 };
 


// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
