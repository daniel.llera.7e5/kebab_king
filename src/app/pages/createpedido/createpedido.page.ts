import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-createpedido',
  templateUrl: './createpedido.page.html',
  styleUrls: ['./createpedido.page.scss'],
})
export class CreatepedidoPage implements OnInit {

  pedidoForm: FormGroup;

  constructor(private dataService: DataService,
    public formBuilder: FormBuilder,   
    private router: Router) { }
 
  ngOnInit() {
    this.pedidoForm = this.formBuilder.group({
      email: [''],
      password: [''],
      longitud: [''],
      latitud: ['']
    })
  }
  onSubmit() {
    if (!this.pedidoForm.valid) {
      return false;
    } else {
      this.dataService.createPedido(this.pedidoForm.value)
      .then(() => {
        this.pedidoForm.reset();
        this.router.navigate(['pedido']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }
 
 

}
