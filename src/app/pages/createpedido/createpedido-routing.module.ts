import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatepedidoPage } from './createpedido.page';

const routes: Routes = [
  {
    path: '',
    component: CreatepedidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatepedidoPageRoutingModule {}
