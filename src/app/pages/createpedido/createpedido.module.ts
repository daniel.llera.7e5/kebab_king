import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatepedidoPageRoutingModule } from './createpedido-routing.module';

import { CreatepedidoPage } from './createpedido.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreatepedidoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreatepedidoPage]
})
export class CreatepedidoPageModule {}
