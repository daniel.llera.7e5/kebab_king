import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {
  pedidos = [];

 constructor(private dataService: DataService,private router: Router) { }


 ngOnInit() {
   this.dataService.getPedidos().subscribe(
     res => {
       this.pedidos = res.map((item) => {
         return {
           id: item.payload.doc.id,
           ... item.payload.doc.data() as Pedido
         };
       })
     }
   );
 }

 deletePedido(id){
  if (window.confirm('Do you want to delete this Pedido?')) {
    this.dataService.deletePedido(id)
  }
}
  navigate_create() {
    this.router.navigate(['createpedido'])
  }
  navigate_atras() {
    this.router.navigate(['home-admin'])
  }
}

export interface Pedido {
  id: string;
  email: string;
  password: string;
  longitud: string;
  latitud: string;

}
