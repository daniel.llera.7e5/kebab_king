import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-producto',
  templateUrl: './edit-producto.page.html',
  styleUrls: ['./edit-producto.page.scss'],
})
export class EditProductoPage implements OnInit {

  editForm: FormGroup;
  id: String;
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
        private router: Router, public formBuilder: FormBuilder) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getProducto(this.id).subscribe(
      res => {
      this.editForm = this.formBuilder.group({
        name: [res['name']],
        price: [res['price']],
        category: [res['category']]

      })
    });
  }
 

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      name: [''],
      price: [''],
      category: ['']
    })
  }
 
  onSubmit() {
    this.dataService.updateProducto(this.id, this.editForm.value);
  }
 
}
