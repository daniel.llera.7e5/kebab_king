import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { environmentmap } from 'src/environments/environment';
declare var mapboxgl: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})

export class MapPage implements OnInit {

  id: String;
  longitud;
  latitud;
  constructor(private dataService: DataService,private activatedRoute: ActivatedRoute,private router: Router) { 
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getPedido(this.id).subscribe(
      res => {
        this.latitud = [res["latitud"]];
        this.longitud = [res["longitud"]];

      }

    )
  }
  

  ngOnInit(): void {
    setTimeout(() => {    
      mapboxgl.accessToken = environmentmap.mapBoxToken
      var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [this.longitud, this.latitud],
        zoom: 14
      });
  
      map.on('load', () => {
        map.resize();
      });
  
      map.addControl(new mapboxgl.NavigationControl());
  
      var marker = new mapboxgl.Marker({})
      .setLngLat([this.longitud, this.latitud])
      .setPopup(new mapboxgl.Popup().setHTML(this.longitud+ " " +this.latitud))
      .addTo(map);
    }, 1000);
   
  }
  navigate_atras() {
    this.router.navigate(['pedido'])
  }
}
