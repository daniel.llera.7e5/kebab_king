import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-pedido',
  templateUrl: './edit-pedido.page.html',
  styleUrls: ['./edit-pedido.page.scss'],
})
export class EditPedidoPage implements OnInit {

  editForm: FormGroup;
  id: String;
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
        private router: Router, public formBuilder: FormBuilder) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getPedido(this.id).subscribe(
      res => {
      this.editForm = this.formBuilder.group({
        email: [res['email']],
        password: [res['password']],
        longitud: [res['longitud']],
        latitud: [res['latitud']]
      })
    });
  }
 
  ngOnInit() {
    this.editForm = this.formBuilder.group({
      email: [''],
      password: [''],
      longitud: [''],
      latitud: ['']
    })
  }
 
  onSubmit() {
    this.dataService.updatePedido(this.id, this.editForm.value);
  }

}
