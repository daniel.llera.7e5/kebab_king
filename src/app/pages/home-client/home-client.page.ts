import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';

@Component({
  selector: 'app-home-client',
  templateUrl: './home-client.page.html',
  styleUrls: ['./home-client.page.scss'],
})
export class HomeClientPage implements OnInit {
  productos = [];

  constructor(private dataService: DataService,private router: Router,private emailComposer: EmailComposer) { }
  ngOnInit() {
    this.dataService.getProductos().subscribe(
      res => {
        this.productos = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Producto
          };
        })
      }
    );
  } 
  order(){
      let email = {
        to: 'dani.santiago@itb.cat', cc: 'daniel.llera.7e5@itb.cat',
        subject: 'Compra Kebab King',
        body: 'Compra Completada con Exito en el mejor Kebab 🌍 🔝', isHtml: true
      }
      this.emailComposer.open(email);
    
    window.location.reload ();
  }

}
export interface Producto {
  id: string;
  name: string;
  price: string;
  category: string;
}