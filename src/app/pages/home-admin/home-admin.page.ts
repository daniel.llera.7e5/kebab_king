import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.page.html',
  styleUrls: ['./home-admin.page.scss'],
})
export class HomeAdminPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigate_productos() {
    this.router.navigate(['producto'])
  }

  navigate_pedidos() {
    this.router.navigate(['pedido'])
  }
}
