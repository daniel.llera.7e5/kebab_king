import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public router: Router,private authService: AuthenticationService) { }

  ngOnInit() {
  }

  logIn(email, password) {
    var element = <HTMLInputElement> document.getElementById("is3dCheckBox");
    var isChecked = element.checked;
    this.authService.signIn(email.value, password.value,)
    console.log(isChecked)
    if (isChecked == true) {
      this.router.navigateByUrl('/home-admin');
    }
    else{
      this.router.navigateByUrl('/home-client');
    }
  }
 
  navigate_registro() {
    this.router.navigate(['register'])
  }
}

