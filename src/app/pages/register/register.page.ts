import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(public router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
  }

  signUp(userName, email, password){
    this.authService.registerUser(userName.value, email.value, password.value)     
    .then((res) => {
      this.authService.signIn(email, password);
      this.router.navigate(['login']);
    }).catch((error) => {
      window.alert(error.message)
    })
  }
 
  navigate_login() {
    this.router.navigate(['login'])
  }

}
