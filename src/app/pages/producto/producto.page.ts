import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { NavigationExtras, Router } from '@angular/router';
@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {
  productos = [];
   
  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {
    this.dataService.getProductos().subscribe(
      res => {
        this.productos = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Producto
          };
        })
      }
    );
  } 
  deleteProducto(id){
    if (window.confirm('Do you want to delete this product?')) {
      this.dataService.deleteProducto(id)
    }
  }
  navigate_atras() {
    this.router.navigate(['home-admin'])
  }
  navigate_create() {
    this.router.navigate(['create-producto'])
  }

}


export interface Producto {
  id: string;
  name: string;
  price: string;
  category: string;
}
