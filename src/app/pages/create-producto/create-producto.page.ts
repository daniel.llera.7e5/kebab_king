import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-create-producto',
  templateUrl: './create-producto.page.html',
  styleUrls: ['./create-producto.page.scss'],
})
export class CreateProductoPage implements OnInit {
  productoForm: FormGroup;

  constructor(private dataService: DataService,
    public formBuilder: FormBuilder,   
    private router: Router) { }

    ngOnInit() {
      this.productoForm = this.formBuilder.group({
        name: [''],
        price: [''],
        category: ['']
      })
    }

    onSubmit() {
      if (!this.productoForm.valid) {
        console.log("out")
      } else {
        this.dataService.createProducto(this.productoForm.value)
        .then(() => {
          this.productoForm.reset();
          this.router.navigate(['producto']);
        }).catch((err) => {
          console.log(err)
        });
      }
    }
   
}
