import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
 {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full',

  },
  {
    path: 'producto',
    loadChildren: () => import('./pages/producto/producto.module').then( m => m.ProductoPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'create-producto',
    loadChildren: () => import('./pages/create-producto/create-producto.module').then( m => m.CreateProductoPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'edit-producto/:id',
    loadChildren: () => import('./pages/edit-producto/edit-producto.module').then( m => m.EditProductoPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'home-admin',
    loadChildren: () => import('./pages/home-admin/home-admin.module').then( m => m.HomeAdminPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'pedido',
    loadChildren: () => import('./pages/pedido/pedido.module').then( m => m.PedidoPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'createpedido',
    loadChildren: () => import('./pages/createpedido/createpedido.module').then( m => m.CreatepedidoPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'edit-pedido/:id',
    loadChildren: () => import('./pages/edit-pedido/edit-pedido.module').then( m => m.EditPedidoPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)

  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'map/:id',
    loadChildren: () => import('./pages/map/map.module').then( m => m.MapPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'home-client',
    loadChildren: () => import('./pages/home-client/home-client.module').then( m => m.HomeClientPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'splash',
    loadChildren: () => import('./splash/splash.module').then( m => m.SplashPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
