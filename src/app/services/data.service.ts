import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Pedido } from '../pages/pedido/pedido.page';
import { Producto } from '../pages/producto/producto.page';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router) { }

  createProducto(producto: Producto){
    return this.firestore.collection('Productos').add(producto);
  }
 
  getProductos() {
    return this.firestore.collection('Productos').snapshotChanges();
  }
 
  getProducto(id) {
    return this.firestore.collection('Productos').doc(id).valueChanges();
  }

  updateProducto(id, producto: Producto) {
    this.firestore.collection('Productos').doc(id).update(producto)
      .then(() => {
        this.router.navigate(['producto']);
      }).catch(error => console.log(error));
  }
 
  deleteProducto(id) {
    this.firestore.doc('Productos/'+id).delete();
  }
 
 //Pedidos

  createPedido(pedido: Pedido){
    return this.firestore.collection('pedidos').add(pedido);
  }
 
  getPedidos() {
    return this.firestore.collection('pedidos').snapshotChanges();
  }
 
  getPedido(id) {
    return this.firestore.collection('pedidos').doc(id).valueChanges();
  }

  updatePedido(id, pedido: Pedido) {
    this.firestore.collection('pedidos').doc(id).update(pedido)
      .then(() => {
        this.router.navigate(['pedido']);
      }).catch(error => console.log(error));
  }
 
  deletePedido(id) {
    this.firestore.doc('pedidos/'+id).delete();
  }
 
 
 
}

